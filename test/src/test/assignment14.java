/*Java Program to reverse a string using Recursive Function*/
package test;

import java.util.*;

public class assignment14
{
    //Driver Code
    public static void main(String[] args) 
    {
        //Initialize the String
        String str = "Reverse String";
        System.out.println("The entered string is: " + str);
        String rev = reverseString(str);
        System.out.println("The reversed string is: " + rev);
    }
    //Recursive Function to Reverse the String
    public void reverseString(int i, int n, int [] arr)
    {
       if(i==n)
       {
         return ;
       } 
       else
       {
         reverseArray(i+1, n, arr);
         System.out.println(arr.at(i));
       }
    }
}