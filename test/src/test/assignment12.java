package test;

import java.util.*;

public class assignment12 {

	public static String rev(String m) {
		String s2 = "";
		for (int i = 0; i < m.length(); i++) {
			s2 = m.charAt(i) + s2;
		}
		return s2 + " ";
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String s=sc.nextLine();
		String[] j = s.split(" ");
		StringBuilder q = new StringBuilder("");
		for (String m : j) {
			// System.out.println(m);
			String l = rev(m);
			// System.out.println(l);

			StringBuilder o = new StringBuilder(l);
			q.append(o);

		}
		System.out.println(q);
	}

}
